/**
 * Meta JS File that will be picked up by the Vue CLI
 */
module.exports = {
  /**
     * Add your own Prompt questions here!
     */
  prompts: {
    name: {
      type: 'string',
      required: true,
      message: 'Project name'
    },
    description: {
      type: 'string',
      required: false,
      message: '',
      default: 'Jino Nuxt Boilerplate'
    },
    author: {
      type: 'string',
      message: 'Author'
    }
  },

  /**
     * You can add a custom complete message
     */
  completeMessage: 'Project Complete!'
}
